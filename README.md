# Egweb
Легковесный веб-фреймворк.

## Требуемые инструменты
- Docker
- Lando

## Разворачивание и запуск
```shell
git clone git@gitlab.com:arcxevodov/egweb.git # Клонирование репозитория
git branch dev # В данный момент все новые изменения находятся на dev-ветке
cp .env.example .env # Инициализация .env
lando start # Сброка образа и запуск контейнера
lando composer update # Обновление composer
```

## Реализовано на данный момент
- Запрос/Ответ
- Маршрутизация и контроллеры
- Dependency Injection
- Интегрирован шаблонизатор Twig

## Активно ведется работа
- CLI
- Интеграция Telegram
- База данных
- Сессии
- Аутентификация
- Middleware
- События
<?php

define("BASE_PATH", dirname(__DIR__));

require_once BASE_PATH . '/vendor/autoload.php';

use Egweb\Framework\Http\Kernel;
use Egweb\Framework\Http\Request;
use League\Container\Container;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

$request = Request::createFromGlobals();

/** @var Container $container */
$container = require BASE_PATH . '/config/services.php';

try {
    $kernel = $container->get(Kernel::class);
} catch (NotFoundExceptionInterface|ContainerExceptionInterface $e) {
    die("Container Exception");
}

$response = $kernel->handle($request);

$response->send();
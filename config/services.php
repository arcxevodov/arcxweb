<?php

use Egweb\Framework\Controller\AbstractController;
use Egweb\Framework\Http\Kernel;
use Egweb\Framework\Router\Router;
use Egweb\Framework\Router\RouterInterface;
use League\Container\Argument\Literal\ArrayArgument;
use League\Container\Argument\Literal\StringArgument;
use League\Container\Container;
use League\Container\ReflectionContainer;
use Symfony\Component\Dotenv\Dotenv;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

// App Params
$routes = require BASE_PATH . '/routes/main.php';

$env = new Dotenv();
$env->load(BASE_PATH . '/.env');
$appEnv = $_ENV['APP_ENV'] ?? 'dev';

$viewsPath = BASE_PATH . '/views';

// App Container
$container = new Container();

$container->add('APP_ENV', new StringArgument($appEnv));

$container->delegate(new ReflectionContainer(true));

$container->add(RouterInterface::class, Router::class);

$container->add(Kernel::class)
    ->addArgument(RouterInterface::class)
    ->addArgument($container);

$container->extend(RouterInterface::class)
    ->addMethodCall('registerRoutes', [new ArrayArgument($routes)]);

$container->addShared('twig-loader', FilesystemLoader::class)
    ->addArgument(new StringArgument($viewsPath));

$container->addShared('twig', Environment::class)
    ->addArgument('twig-loader');

$container->inflector(AbstractController::class)
    ->invokeMethod('setContainer', [$container]);

return $container;
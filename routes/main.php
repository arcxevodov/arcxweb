<?php

use App\Controllers\HomeController;
use Egweb\Framework\Router\Route;

return [
    Route::get('/', [HomeController::class, 'home']),
];